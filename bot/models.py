from django.db import models

categories = (
    ('grills', 'grills'),
    ('pastry', 'pastry'),
)

class Meals(models.Model): 
    
    name = models.CharField(max_length=20 ,blank=True, null=True)
    price = models.IntegerField(default=0)
    category = models.CharField(max_length=20 ,blank=True, null=True, choices=categories)


    class Meta:
        verbose_name = "Meals"


    def __str__(self):
        return self.name
