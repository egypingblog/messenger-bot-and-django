# Generated by Django 3.1.4 on 2020-12-24 21:15

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Meals',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=20, null=True)),
                ('stock', models.IntegerField(default=0)),
                ('category', models.CharField(blank=True, max_length=20, null=True)),
            ],
            options={
                'verbose_name': 'Meals',
            },
        ),
    ]
