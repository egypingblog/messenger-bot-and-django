import json, requests, random, re

from .models import Meals
from django.views import generic
from django.http.response import HttpResponse

from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils.decorators import method_decorator

#  ------------------------ Fill this with your page access token! -------------------------------
PAGE_ACCESS_TOKEN = "FB Page token here"
VERIFY_TOKEN = "2318934571"


'''
{
  "object": "page",
  "entry": [
    {
      "id": "137101496360273",
      "time": 1489404570380,
      "messaging": [
        {
          "sender": {
            "id": "3813657865350990"
          },
          "recipient": {
            "id": "xyz"
          },
          "timestamp": 1489404570341,
          "message": {
            "mid": "mid.1489404570341:ce017e2f89",
            "seq": 72281,
            "text": "stupid"
          }
        }
      ]
    }
  ]
}
'''


def post_facebook_message(fbid, recevied_message):
    pastries_menu = Meals.objects.filter(category="pastry")
    grills_menu = Meals.objects.filter(category="grills")


    user_text = re.sub(r"[^a-zA-Z0-9\s]",' ',recevied_message).lower().split()
    return_message = ''

    for text in user_text:
        if text == "pastry":
            for meal in pastries_menu:
                meal = str(meal.name) + " " + str(meal.price) + "$" + "\n"
                return_message += meal
            break
        
        elif text == "grills":
            for meal in grills_menu:
                meal = str(meal.name) + " " + str(meal.price) + "$" + "\n"
                return_message += meal
            break

    if not return_message:
        return_message = "Welcome to PythonArabia restaurant \n Do you want PASTRY or GRILLS ??" 

    post_message_url = 'https://graph.facebook.com/v2.6/me/messages?access_token=%s'%PAGE_ACCESS_TOKEN
    response_msg = json.dumps({"recipient":{"id":fbid}, "message":{"text":return_message}})
    print(response_msg)
    status = requests.post(post_message_url, headers={"Content-Type": "application/json"},data=response_msg)
    print(status.json())


@method_decorator(csrf_exempt, name='dispatch')
class BotAPI(generic.View):
    def get(self, request, *args, **kwargs):
        if self.request.GET['hub.verify_token'] == VERIFY_TOKEN:
            return HttpResponse(self.request.GET['hub.challenge'])
        else:
            return HttpResponse('Error, invalid token')
        
    def post(self, request, *args, **kwargs):
        fb_message = json.loads(self.request.body.decode('utf-8'))
        for entry in fb_message['entry']:
            for message in entry['messaging']:
                if 'message' in message:
  
                    post_facebook_message(message['sender']['id'], message['message']['text'])    
        return HttpResponse()