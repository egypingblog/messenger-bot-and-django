from django.urls import include, path
from .views import BotAPI


urlpatterns = [
    path('botget', BotAPI.as_view()) 
]